//
//  MaterialView.swift
//  DreamList
//
//  Created by Lucas Moraes on 12/01/17.
//  Copyright © 2017 Lucas Moraes. All rights reserved.
//

import UIKit

private var materialKey = false

extension UIView {

    @IBInspectable var materialDesign: Bool {
        get {return materialKey}
        set {
            materialKey = newValue
            if materialKey == true {
                self.layer.masksToBounds = false
                self.layer.cornerRadius = 3.0
                self.layer.shadowOpacity = 0.8
                self.layer.shadowRadius = 3.0
                self.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
                self.layer.shadowColor = UIColor(red: 201/255, green: 201/255, blue: 201/255, alpha: 1.0).cgColor
                
            } else {
                self.layer.cornerRadius = 0
                self.layer.shadowOpacity = 0
                self.layer.shadowRadius = 0
                self.layer.shadowColor = nil
            }
        } 
    }
    

}
